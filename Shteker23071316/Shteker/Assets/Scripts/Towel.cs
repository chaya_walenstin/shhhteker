﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Towel : MonoBehaviour {
    private Camera camera;
    private Rigidbody2D rigidbody2D;
    private Transform transform;
    int countDrops = 0;
    // Use this for initialization
    void Start () {
       
        transform = GetComponent<Transform>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        camera = FindObjectOfType<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnMouseDrag()
    {
        Vector3 mouseScreenPosition = Input.mousePosition;
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(mouseScreenPosition);
        mouseWorldPosition.z = 0;
        transform.position = mouseWorldPosition;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(countDrops == 40)
        {
            SceneManager.LoadScene(2);
        }
        print(collision.tag);
        if (collision.gameObject.tag == "Drop")
        {
            gameObject.GetComponent<AudioSource>().Play();
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = -889;
            collision.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            countDrops++;
        }
       
    }
}
