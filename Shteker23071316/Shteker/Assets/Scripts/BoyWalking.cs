﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoyWalking : MonoBehaviour
{


    public Camera camera;
    private int timer;
    private Rigidbody2D rigidbody2D;
    private Animator animator;
    private Transform transform;
 
    public RuntimeAnimatorController anim1;
    public RuntimeAnimatorController anim2;
    public float speed = 4;
    bool shoe1 = false;
    bool shoe2 = false;
    // Use this for initialization
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        transform = GetComponent<Transform>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody2D.freezeRotation = true;
        rigidbody2D.velocity = new Vector2(speed, 0.2f);


    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RedShoes")
        {
            shoe1 = true;
        
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = -5;
            if (shoe1)
            {
                animator.runtimeAnimatorController = anim1;
            }
            if (shoe1 && shoe2)
            {
                animator.runtimeAnimatorController = anim2;
                Invoke("Success", 1f);
            }

        }
        else
        {
            if (collision.gameObject.tag == "RedShoes1")
            {
                shoe2 = true;

                collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = -5;
                if (shoe2)
                {
                    animator.runtimeAnimatorController = anim1;
                }
                if (shoe1 && shoe2)
                {
                    animator.runtimeAnimatorController = anim2;
                    Invoke("Success", 1f);
                }

            }
            else
            {
                if (collision.gameObject.tag == "Danger")
                {
                    SceneManager.LoadScene(1);
                }
                else
                    collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = 5;
            }
        }
       
    }
    void Success()
    {
        SceneManager.LoadScene(2);
    }

}
