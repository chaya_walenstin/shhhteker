﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BathFall : MonoBehaviour {
    private Transform transform;
    public GameObject[] tools;
    int timer = 60;
    int i = 1;
    int i1;
    // Use this for initialization
    void Start () {

        transform = GetComponent<Transform>();
        RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider();
        GameObject[] tools1 = tools.OrderBy(x => GetNextInt32(rnd)).ToArray();
        for (i = 0; i < 22; i++)
        {
            int j = 22 - i;
           
                Instantiate(tools1[i], transform.position - new Vector3(0, j*2, 0), Quaternion.identity);
            print(i);
            
               
            

        }
        
    }

    private object GetNextInt32(RNGCryptoServiceProvider rnd)
    {
        byte[] randomInt = new byte[4];
        rnd.GetBytes(randomInt);
        return Convert.ToInt32(randomInt[0]);
    }


    // Update is called once per frame
    void Update () {
        for (i1 = 0; i1 < 21; i1++)
        {
            if (tools[i1].GetComponent<Transform>().position.y < -1000)
                continue;
        }
        if (i1 == 22)
        {
            SceneManager.LoadScene(2);
        }


    }
  
}
