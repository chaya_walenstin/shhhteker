﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShekaMovement : MonoBehaviour
{
    public bool covared = false;
    private Rigidbody2D rigidbody2D;
    private Transform transform;
    float force = 3;
    private float speed = 3;
    private AudioSource audioSource;
    private SpriteRenderer spriteRenderer;
    public Sprite sprite;
    public AudioClip audio1;
    public AudioClip audio2;
    // Use this for initialization
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        transform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {


            Right();

        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {


            Left();

        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {

            Up();


        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {

            Down();

        }


    }

    public void Left()
    {
        spriteRenderer.flipX = true;
        rigidbody2D.velocity = new Vector2(-speed, rigidbody2D.velocity.y);
     
    }
    public void Right()
    {
        spriteRenderer.flipX = false;
        rigidbody2D.velocity = new Vector2(speed, rigidbody2D.velocity.y);
    }
    public void Up()
    {
        spriteRenderer.flipX = false;
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, speed);
    }
    public void Down()
    {

        spriteRenderer.flipX = true;
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, -speed);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Cover")
        {
            audioSource.clip = audio1;
            audioSource.Play();
            collision.transform.parent = gameObject.GetComponent<Transform>();
            covared = true;
        }
        if(collision.gameObject.tag == "Shekaa")
        {
            if (covared)
            {
                audioSource.clip = audio2;
                audioSource.Play();
                collision.gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                Invoke("Success", 1f);
            }
            else
            {
                SceneManager.LoadScene(1);
            }
        }
    }
    private void Success()
    {
        SceneManager.LoadScene(2);
    }
}
