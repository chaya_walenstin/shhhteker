﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {
    private void Start()
    {
        Invoke("MoveToScene", 4f);
    }

    public void MoveToScene()
    {
        SceneManager.LoadScene(Lives.currentLevel);
    }
}
