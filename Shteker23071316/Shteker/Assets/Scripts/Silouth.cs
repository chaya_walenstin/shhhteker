﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Silouth : MonoBehaviour {
    public Sprite sprite;
    public SpriteRenderer sr;
    private bool saved;
    public LeveManager leveManager;
    private AudioSource audioSource;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == gameObject.tag)
        {
            audioSource.Play();
            if (!saved)
            {
                leveManager.Increase();
            }
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
            sr.enabled = false;
            saved = true;
          
        }
    }
}
