﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Saved : MonoBehaviour {
    public GameObject shteker;
    private ShtekerMovement shtekerMovement;
    private Rigidbody2D rigidbody2D;
	// Use this for initialization
	void Start () {
        shtekerMovement = shteker.GetComponent<ShtekerMovement>();
        rigidbody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        rigidbody2D.freezeRotation = true;
        //collision.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
        shtekerMovement.enabled = false;
        //shteker.GetComponent<Rigidbody2D>().gravityScale = 1;
        if (collision.gameObject.tag == "Shteker")
            Invoke("Success", 1f);
        else
        {
            gameObject.GetComponent<AudioSource>().Play();
            Invoke("Silance", 0.4f);
        }
    }
    void Silance()
    {
     
        gameObject.GetComponent<AudioSource>().mute = true;
    }
    private void Success()
    {
        SceneManager.LoadScene(2);
    }
}
