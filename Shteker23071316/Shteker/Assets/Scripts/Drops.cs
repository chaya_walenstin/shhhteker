﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drops : MonoBehaviour {
    private Transform transform;
    public GameObject drop;
    public float h = 2;
    public float iteration = 7;
    int timer = 60;
    int i = 1;
    // Use this for initialization
    void Start () {
        transform = GetComponent<Transform>();
        
        
         


            InvokeRepeating("DropFalling", iteration, iteration);

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void DropFalling()
    {
        Instantiate(drop, transform.position, Quaternion.identity);
    }
}
