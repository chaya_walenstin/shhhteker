﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolsMovement : MonoBehaviour {
    private Rigidbody2D rigidbody2D;
    float force = 0;
    private Camera camera;
    private AudioSource audioSource;

    private float speed = 30;

    // Use this for initialization
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.gravityScale = 0;

        force = Random.Range(2, 4);
        speed = Random.Range(10, 40);
        float x = Random.Range(-1, 1);
        if(x<0)
            rigidbody2D.AddForce(Vector2.left * speed);  
        else
          
            rigidbody2D.AddForce(Vector2.right * speed);

      
        rigidbody2D.velocity = Vector2.up * force;
        


        camera = FindObjectOfType<Camera>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
		
	}
    private void OnMouseDrag()
    {
        Vector3 mouseScreenPosition = Input.mousePosition;
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(mouseScreenPosition);
        mouseWorldPosition.z = 0;
       
        Debug.Log(Input.mousePosition.x);
        Debug.Log(gameObject.transform.position.x);
        if (transform.position.x > mouseWorldPosition.x && gameObject.tag == "Kitchen")
        {
            audioSource.Play();
            transform.position = mouseWorldPosition;
            rigidbody2D.gravityScale = 0;
            rigidbody2D.AddForce(Vector2.left * 2000);
            rigidbody2D.AddForce(Vector2.down * 700);
            FallManager.success++;
        }
        if (transform.position.x < mouseWorldPosition.x && gameObject.tag == "Room")
        {
            audioSource.Play();
            transform.position = mouseWorldPosition;
            rigidbody2D.gravityScale = 0;
            rigidbody2D.AddForce(Vector2.right * 2000);
            rigidbody2D.AddForce(Vector2.down * 700);
            FallManager.success++;
        }
    }

}
