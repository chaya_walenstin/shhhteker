﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour {
    private AudioSource audioSource;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void NewGame1()
    {
        Lives.health = 3;
        Lives.currentLevel = 3;
        SceneManager.LoadScene(0);
    }
    public void Play()
    {
        audioSource.Play();
    }
}
