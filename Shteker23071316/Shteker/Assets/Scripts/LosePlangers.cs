﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LosePlangers : MonoBehaviour {

    private Transform transform;
    public Rigidbody2D rigidbody2D;
    private AudioSource audioSource;
    int i;
    float i1 = 0;
    int plangerCounter = 0;
    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        transform = GetComponent<Transform>();
        for (i = 0; i < Lives.health; i++)
        {

            Invoke("Plangers", i + 0.50f);

           
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
    void Plangers()
    {
        print(plangerCounter + " " + Lives.health);
        plangerCounter++;
        Instantiate(rigidbody2D, transform.position + new Vector3(i1, 0, 0), Quaternion.identity);
        i1 = i1 + 2.1f;
        if (plangerCounter == Lives.health)
        {
            audioSource.Play();
            rigidbody2D.gravityScale = -4;
            
        }

    }
}
