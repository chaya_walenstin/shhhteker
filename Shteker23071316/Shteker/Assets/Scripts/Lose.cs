﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Lose : MonoBehaviour {

	// Use this for initialization
	void Start () {
     
        Lives.health--;
        print("h" + Lives.health);
        if (Lives.health <= 0)
        {
            print("GameOver");
            Invoke("Over", 4f);

        }
        else
        {
            print(Lives.health);
            if (Lives.currentLevel < 9)
            {
                Lives.currentLevel++;
            }
            Invoke("Menu", 4f);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void Menu()
    {
        SceneManager.LoadScene(0);
    }
    void Over()
    {
        SceneManager.LoadScene(10);
    }
}
