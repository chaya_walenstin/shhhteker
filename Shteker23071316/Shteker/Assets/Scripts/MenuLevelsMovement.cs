﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLevelsMovement : MonoBehaviour {
    public Transform[] levels;
    private Transform transform;
    private Rigidbody2D rigidbody2D;
    Vector3 nextLevelPosition = new Vector3();
	// Use this for initialization
	void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        transform = GetComponent<Transform>();
        if(Lives.currentLevel>3)
            transform.position = levels[Lives.currentLevel - 4].position;
        if (Lives.currentLevel < 10)
        {
            nextLevelPosition = levels[Lives.currentLevel - 3].position;
            print(nextLevelPosition);
        }
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.x< nextLevelPosition.x)
        {
            print(transform.position.x + " " + nextLevelPosition.x);
            rigidbody2D.velocity = Vector3.right * 2;
        }
        else
        {
            rigidbody2D.velocity = Vector2.zero;
            
        }
	}
}
