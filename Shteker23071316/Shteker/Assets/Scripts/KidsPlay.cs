﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KidsPlay : MonoBehaviour {
    public Camera camera;
    private int timer;
    private Rigidbody2D rigidbody2D;
    private Transform transform;
    private Animator animator;
    public float speed = 4;
    // Use this for initialization
    void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        transform = GetComponent<Transform>();
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        rigidbody2D.freezeRotation = true;
        rigidbody2D.velocity = new Vector2(1-speed, 0.2f);
    
    }
    private void OnMouseDrag()
    {
        Vector3 mouseScreenPosition = Input.mousePosition;
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(mouseScreenPosition);
        mouseWorldPosition.z = 0;
        transform.position = mouseWorldPosition;
        animator.enabled = false;
    }
    private void Success()
    {
        SceneManager.LoadScene(2);
    }
}
