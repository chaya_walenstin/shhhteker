﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ShtekerMovement : MonoBehaviour {

    private Rigidbody2D rigidbody2D;
    float speed = 4;
    public Camera camera;
    private Transform transform;
    private Animator animator;
    public GameObject timerSprite;
    private SpriteRenderer spriteRenderer;
    public SpriteRenderer success;
    public Transform gameObject;
    public RuntimeAnimatorController idle;
    public SpriteRenderer squere;

	// Use this for initialization
	void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        transform = GetComponent<Transform>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        //squere.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        rigidbody2D.freezeRotation = true;
        rigidbody2D.velocity = new Vector2(1 - speed, rigidbody2D.velocity.y);
	}
    private void OnMouseDrag()
    {
        //rigidbody2D.isKinematic = true;
        //gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
        rigidbody2D.freezeRotation = true;

        squere.GetComponent<Rigidbody2D>().gravityScale = 80;
    
        animator.runtimeAnimatorController = idle;
        Vector3 mouseScreenPosition = Input.mousePosition;
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(mouseScreenPosition);
        //Debug.Log(mouseWorldPosition);
        rigidbody2D.gravityScale = 1;
        mouseWorldPosition.z = 0;
        transform.position = mouseWorldPosition;
        Invoke("Saved", 1);
        

    }

    /// <summary>
    /// 
    ///  כששטקר ניצל, אחרי שניה אמור להופיע איזשהו ספרייט שמריע, אחר כך עוברים לעמוד ההצלחה, כרגע יש ספרייט אחר.
    /// </summary>
    private void Saved()
    {
        
        timerSprite.GetComponent<SpriteRenderer>().enabled = false;
        timerSprite.GetComponent<AudioSource>().mute = true;
        ///Instantiate(success, gameObject.position, Quaternion.identity);
        /// spriteRenderer.enabled = false;
        ///   Invoke("Success", 4);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Danger")
        {
            print("hhhhhhhhhh");
      
            Invoke("Lose", 0);
        }
        
    }
    private void Lose()
    {
        SceneManager.LoadScene(1);
    }
}
