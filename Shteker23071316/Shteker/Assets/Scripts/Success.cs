﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Success : MonoBehaviour {

	// Use this for initialization
	void Start () {
     
        Lives.currentLevel++;
        if (Lives.currentLevel > 9)
        {
            Invoke("Win", 4f);
         
        }
        Invoke("Menu", 4f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void Menu()
    {
        SceneManager.LoadScene(0);
    }
    void Win()
    {
        SceneManager.LoadScene(11);
    }
}
