﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DropMovement : MonoBehaviour {
    private Rigidbody2D rigidbody2D;
    private float force = 3;
   
    // Use this for initialization
    void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.velocity = Vector2.down * force;
    }
	
	// Update is called once per frame
	void Update () {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Danger")
        {
            gameObject.GetComponent<AudioSource>().Play();
            SceneManager.LoadScene(1);
        }
    }

   
}
