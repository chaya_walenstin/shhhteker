﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoes : MonoBehaviour {
    private Camera camera;
    private Rigidbody2D rigidbody2D;
    private Transform transform;
    
    // Use this for initialization
    void Start () {
        transform = GetComponent<Transform>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        camera = FindObjectOfType<Camera>();
    }
    private void OnMouseDrag()
    {
        Vector3 mouseScreenPosition = Input.mousePosition;
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(mouseScreenPosition);
        mouseWorldPosition.z = 0;
        transform.position = mouseWorldPosition;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
