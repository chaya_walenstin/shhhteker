﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BirdMovement : MonoBehaviour {
    private Rigidbody2D rigidbody2D;
    private float speed = 15;
    private float force = 8;
    private Transform transform;
    public GameObject parent;
    private int count = 0;
    // Use this for initialization
    void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        transform = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
        
        rigidbody2D.velocity = new Vector2(speed, rigidbody2D.velocity.y);
        if (Input.GetKey(KeyCode.Space))
        {
            Fly();
        }
        if (transform.position.x > 100 && count >=10)
        {
            Success();
        }
        else
        {
            if (transform.position.x > 100 && count < 10)
            {
                Lose();
            }
        }
	}
    public void Fly()
    {
        rigidbody2D.velocity = new Vector2( rigidbody2D.velocity.x, force);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Kite")
        {
            collision.gameObject.GetComponent<AudioSource>().Play();
            count++;
            print(count);
            collision.isTrigger = true;
            collision.gameObject.AddComponent<Rigidbody2D>();
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = -1f;
            //collision.transform.parent = parent.GetComponent<Transform>();
        }
    }
    private void Success()
    {
        SceneManager.LoadScene(2);
    }
    private void Lose()
    {
        SceneManager.LoadScene(1);
    }
}
